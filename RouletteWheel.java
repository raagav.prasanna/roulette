import java.util.Random;
public class RouletteWheel {
    private Random randomizer;
    private int lastSpinVal;

    public RouletteWheel() {
        this.randomizer = new Random();
        this.lastSpinVal = 0;
    }

    public void spin() {
        this.lastSpinVal = randomizer.nextInt(38);
    }

    public int getValue() {
        return(this.lastSpinVal);
    }
}